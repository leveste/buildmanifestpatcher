using System;
using System.IO;
using System.Text;

namespace MyFile
{
	class BinFile{
		public byte[] read_file(string filename){
			byte[] br = File.ReadAllBytes(filename);
			return br;
		}

		public void write_file(string filename, byte[] data){
			File.WriteAllBytes(filename, data);
		}

		public void backup_build_manifest(string filename){
			try{
				File.Copy(filename, filename + ".bck");
			}
			catch (IOException copyError){
				Console.WriteLine(copyError.Message);
			}
		}
	}
}
