#!/usr/bin/env python3
 
from io import BytesIO
from os import urandom
from hashlib import md5, sha1
from json import loads, dumps
from os.path import join, isfile, getsize
 
"""
Windows:
py -3 -m pip install pycryptodome
python -m pip install pycryptodome
pip install pycryptodome
 
Linux:
python3 -m pip install pycryptodome
pip3 install pycryptodome
"""
from Crypto.Cipher import AES
 
# Steam keys
BUILD_MANIFEST_AES_KEY_STEAM = bytes.fromhex("8B031F6A24C5C4F3950130C57EF660E9")
BUILD_MANIFEST_PUB_KEY_STEAM = bytes.fromhex("EF304E27EF83F93E4A0948026D4B58121BEC87BB3A4D179E024C5276DBB0FCA1")
 
# Bethesda.net keys
BUILD_MANIFEST_AES_KEY_BNET = bytes.fromhex("2C05001C1BF0134D1B17D4D5C4D784C0")
BUILD_MANIFEST_PUB_KEY_BNET = bytes.fromhex("71D7AA13804D85C1AB6A5F73683A28F3D8D476823609920A27292496692DF7AE")
 
 
BUILD_MANIFEST_JSON_FILE = "build-manifest.json"
BUILD_MANIFEST_BIN_FILE = "build-manifest.bin"
 
def read_file(filename: str, text: bool = False) -> (bytes, str):
	with open(filename, "r" if text else "rb") as f:
		data = f.read()
	return data
 
def write_file(filename: str, data: (str, bytes, bytearray)) -> None:
	with open(filename, "w" if type(data) == str else "wb") as f:
		f.write(data)
        
def backup_build_manifest() -> None:
    filename = BUILD_MANIFEST_BIN_FILE + ".bck"
    data = read_file(BUILD_MANIFEST_BIN_FILE)
    write_file(filename, data)
 
def ida_hex(data: (bytes, bytearray)) -> str:
	return " ".join([hex(x)[2:].upper() for x in data])
 
def decrypt_build_manifest(data: (bytes, bytearray)) -> str:
	size = len(data)
	with BytesIO(data) as bio:
		gcm_nonce = bio.read(0xC)
		bm_data_enc = bio.read(size - 0xC - 0x50)
		gcm_tag = bio.read(0x10)
		ed25519_sig = bio.read(0x40)
 
	try:
		cipher = AES.new(BUILD_MANIFEST_AES_KEY_STEAM, AES.MODE_GCM, nonce=gcm_nonce, mac_len=0x10)
		cipher.update(b"build-manifest")
		bm_data_dec = cipher.decrypt_and_verify(bm_data_enc, gcm_tag)
	except:
		cipher = AES.new(BUILD_MANIFEST_AES_KEY_BNET, AES.MODE_GCM, nonce=gcm_nonce, mac_len=0x10)
		cipher.update(b"build-manifest")
		bm_data_dec = cipher.decrypt_and_verify(bm_data_enc, gcm_tag)
 
	return bm_data_dec.decode("UTF8")
 
def encrypt_build_manifest(data: (bytes, bytearray, str)) -> bytes:
	if type(data) == str:
		data = data.encode("UTF8")
 
	gcm_nonce = urandom(0xC)
 
	try:
		cipher = AES.new(BUILD_MANIFEST_AES_KEY_STEAM, AES.MODE_GCM, nonce=gcm_nonce, mac_len=0x10)
		cipher.update(b"build-manifest")
		(bm_data_enc, gcm_tag) = cipher.encrypt_and_digest(data)
	except:
		cipher = AES.new(BUILD_MANIFEST_AES_KEY_BNET, AES.MODE_GCM, nonce=gcm_nonce, mac_len=0x10)
		cipher.update(b"build-manifest")
		(bm_data_enc, gcm_tag) = cipher.encrypt_and_digest(data)
 
	bm_data_enc += gcm_tag
	bm_data_enc += b"\x00" * 0x40
	bm_data_enc = gcm_nonce + bm_data_enc
 
	return bm_data_enc
 
def optimize(bm_json: dict) -> dict:
    for (filename, value) in bm_json["files"].items():
        value["hashes"] = ["e2df1b2aa831724ec987300f0790f04ad3f5beb8"]
        if isfile(filename):
            value["fileSize"] = getsize(filename)
        if value["fileSize"] > 4294967295:
            hashes = -(value["fileSize"] // -4294967295)  # Ceiling division
            for i in range(hashes - 1):
                value["hashes"].append("e2df1b2aa831724ec987300f0790f04ad3f5beb8")
        value["chunkSize"] = 4294967295
    return bm_json
 
def main() -> None:
 
	bm_enc = read_file(BUILD_MANIFEST_BIN_FILE)
	bm_dec = decrypt_build_manifest(bm_enc)
	bm_json = optimize(loads(bm_dec))
 
	if not isfile(BUILD_MANIFEST_BIN_FILE + ".bck"):
		backup_build_manifest()
 
	write_file(BUILD_MANIFEST_BIN_FILE, encrypt_build_manifest(dumps(bm_json, separators=(',', ':'))))
 
if __name__ == "__main__":
	main()

